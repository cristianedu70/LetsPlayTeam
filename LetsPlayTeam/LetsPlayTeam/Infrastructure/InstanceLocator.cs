﻿

namespace LetsPlayTeam.Infrastructure
{
    using ViewModels;
    public class InstanceLocator
    {
        public MainViewModel Main { get; set; } //es una instancia de main.
        public InstanceLocator()
        {
            Main = new MainViewModel();
        }
    }
}
